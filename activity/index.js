

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


txtFirstName.addEventListener('keyup', (event)=>{
	showFullName();
});

txtLastName.addEventListener('keyup', (event)=>{
	showFullName();
});

function showFullName(){
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}