/*document.getElementById('txt-first-name');
document.getElementByClassName('txt-last-name');
document.getElementByTagName('input');

document.querySelector('#txt-first-name');*/

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// EVENT LISTENERS
/*
https://www.w3schools.com/jsref/dom_obj_event.asp
https://developer.mozilla.org/en-US/docs/Web/Events
*/
txtFirstName.addEventListener('keyup', (event)=>{
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event)=>{
	console.log(event.target);
	console.log(event.target.value);
});